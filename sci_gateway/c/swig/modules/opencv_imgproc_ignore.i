// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises

%ignore FilterEngine;
%ignore Algorithm;
%ignore Moments;
%ignore CvConnectedComp;
%ignore CvSubdiv2DPointLocation;
%ignore CvNextEdgeType;
%ignore CvHuMoments;
%ignore CvSubdiv2DPoint;
%ignore CvQuadEdge2D;
%ignore CvMoments;
%ignore CvChainPtReader;
%ignore CvConvexityDefect;
%ignore CvFeatureTree;
%ignore CvLSH;
%ignore CvSubdiv2D;
%ignore CvHuMoments;
%ignore CvConnectedComp;
%ignore CvLSHOperations;
