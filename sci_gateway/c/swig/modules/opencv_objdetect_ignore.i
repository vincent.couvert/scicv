// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises

// TODO: fix Detector
%ignore Detector;
%ignore HOGDescriptor;
%ignore CvDataMatrixCode;
%ignore cvLatentSvmDetectObjects;
%ignore CvObjectDetection;
%ignore LatentSvmDetector;
%ignore Data;
%ignore cv::linemod::colormap;
%rename (HaarStgClsf) CvHaarStageClassifier;
%rename (HaarClsfrCasd) CvHaarClassifierCascade;
%rename (LSVMFilterPos) CvLSVMFilterPosition;
%rename (LSVMFilterObj) CvLSVMFilterObject;
%rename (LatentSvmDet) CvLatentSvmDetector;
//%rename (HOGDesc) HOGDescriptor;
