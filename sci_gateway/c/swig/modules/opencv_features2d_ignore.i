// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises

%ignore FlannBasedMatcher;
%ignore HammingMultilevel;
%ignore BOWTrainer;
%ignore BOWImgDescriptorExtractor;
%ignore BOWImgDescriptorExtractor;
%ignore GenericDescriptorMatcher;
%ignore VectorDescriptorMatcher;
%ignore BriefDescriptorExtractor;
%ignore FREAK;

// ignore functions that conflict with Scilab
%ignore write(FileStorage& fs, const string& name, const vector<KeyPoint>& keypoints);
%ignore read(const FileNode& node, vector<KeyPoint>& keypoints);
